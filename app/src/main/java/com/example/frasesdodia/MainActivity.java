package com.example.frasesdodia;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


    private TextView textoNovaFrase;
    private Button botaoNovaFrase;

    private String[] frases =
            {"O garoto apanhou da vizinha, e a mãe furiosa foi tomar satisfação: Por que a senhora bateu no meu filho? Ele foi mal-educado, e me chamou de gorda. E a senhora acha que vai emagrecer batendo nele?"
            ,"Conversa de casados: Querido, o que você prefere? Uma mulher bonita ou uma mulher inteligente? Nem uma, nem outra. Você sabe que eu só gosto de você.",
           "Doutor, como eu faço para emagrecer? Basta a senhora mover a cabeça da esquerda para a direita e da direita para a esquerda. Quantas vezes, doutor? Todas as vezes que lhe oferecerem comida.",
            "A mulher comenta com o marido: Querido, hoje o relógio caiu da parede da sala e por pouco não bateu na cabeça da mamãe... Maldito relógio. Sempre atrasado...",
            "O condenado à morte esperava a hora da execução, quando chegou o padre: Meu filho, vim trazer a palavra de Deus para você. Perda de tempo, seu padre. Daqui a pouco vou falar com Ele, pessoalmente. Algum recado?",
            "Um advogado e sua sogra estão em um edifício em chamas. Você só tem tempo pra salvar um dos dois. O que você faz? Você vai almoçar ou vai ao cinema?",
            "Mamãe, mamãe... na escola me chamaram de mentiroso. Cale-se que você nem vai à escola ainda...",
            "Dois amigos conversam sobre as maravilhas do Oriente. Um deles diz: Quando completei 25 anos de casado, levei minha mulher ao Japão. Não diga? E o que pensa fazer quando completarem 50? Volto lá para buscá-la.",
             "Um baiano deitado na rede pergunta pro amigo: Meu rei... tem aí remédio pra picada de cobra? Tem não, meu lindo. Por que, você foi picado? Não, mas tem uma cobra vindo na minha direção.",
             "Dois amigos se encontram depois de muitos anos. Casei, separei e já fizemos a partilha dos bens. E as crianças? O juiz decidiu que ficariam com aquele que mais bens recebeu. Então ficaram com a mãe? Não, ficaram com nosso advogado.",
             "Se você está se sentindo sozinho, abandonado, achando que ninguém liga para você... “Atrase um pagamento\"",
                    "P: Por que a loira fala ao telefone deitada? R: Para não cair a ligação!!,",
                    "Mamãe, mamãe... me leva no circo? Não, filho... Se querem te ver, que venham aqui em casa..",
                    "Como é que se chama um traficante armado até os dentes? É melhor chamar de senhor...",
                    "O que é preciso para reunir os Beatles? Mais duas balas.",
                    "Por que Hitler odiava os judeus? Porque não conhecia os argentinos.",
                    "O que fala o livro de Matemática para o livro de História? R: Não me venha com história que eu já estou cheio de problema!",
                    "Porque o louco toma banho com o chuveiro desligado? Por que ele comprou um champô para cabelos secos!"

            };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        textoNovaFrase = findViewById(R.id.textoNovaFraseID);
        botaoNovaFrase = findViewById(R.id.BotaoNovaFraseId);


        botaoNovaFrase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();
                int numeroArray = random.nextInt(frases.length);
                textoNovaFrase.setText(frases[numeroArray]);

                botaoNovaFrase.setText("Contar outra");
            }
        });
    }

}